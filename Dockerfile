FROM node:8

RUN apt-get update && \
    apt-get install -y bluez

WORKDIR /usr/src/app
COPY package*.json ./

RUN npm install node-gyp && \
    npm install

COPY ./ /usr/src/app/
EXPOSE 3000

CMD ["npm", "run", "dev" ]
