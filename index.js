//import {SERVER_NAME} from "./src/config/config";
let express = require('express'),
    app = express();

const Magasin = require('./src/models/magasin');
const Client = require('./src/models/client');
const Advertisement = require('./src/models/advertisement');

const {PORT, SERVER_NAME} = require('./src/config/config');

const Logger = require('./src/tools/logger');
let logger = new Logger('APP');

let bodyParser = require('body-parser');
let jsonParser = bodyParser.json({type: 'application/json'});
let cors = require('cors');

app.use(jsonParser);
app.use(cors());


initModules();

async function initModules() {
    try {
        logger.log(SERVER_NAME + " STARTING");
        await require('./src/modules/db.module').init();
        await require('./src/modules/socket.module').init();
        await require('./src/modules/bluetooth.module').init();
        require('./src/modules/routes.module')(app);
        app.listen(PORT | 3000, () => {
            logger.logDone('SERVER LISTENING ON ' + (PORT | 3000));
        });
    } catch (err) {
        logger.logError(err);
    }
}
