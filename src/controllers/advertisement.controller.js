let advertisementBuilder = require('../builders/advertisement.builder');
let bluetoothModule = require('../modules/bluetooth.module');
let clientBldr = require('../builders/client.builder');
let clientCtlr = require('../controllers/client.controller');
let Logger = require('../tools/logger');
let logger = new Logger('ADVERTISEMENT');

exports.list = function (req, res) {
    logger.log('LIST');
    advertisementBuilder.listAll().exec((err, advertisements) => {
        if (err) return logger.log(err);
        res.send(advertisements);
    });
};

exports.read = function (req, res) {
    logger.log('READ');
    advertisementBuilder.read(req.params.id).exec((err, advertisement) => {
        if (err) return logger.log(err);
        res.send(advertisement);
    });
};


exports.create = function (req, res) {
    logger.log('CREATE');
    let newadvertisement = req.body;
    advertisementBuilder.create(newadvertisement, (err, advertisement) => {
        if (err) return logger.log(err);
        res.send(advertisement);
    });
};

exports.update = async function (req, res) {
    try {
        logger.log('UPDATE ' + req.params.id);
        let updatedadvertisement = req.body;
        let advertisement = await advertisementBuilder.update(updatedadvertisement)
        if (advertisement.isActive) {
            let presentTargets = await bluetoothModule.getDatas();
            let mac_addresses = [];
            console.log(presentTargets);
            presentTargets.forEach(target => mac_addresses.push(target.mac_address));
            let presentClients = await clientBldr.getByManyMacAddress(mac_addresses);
            console.log(presentClients);
            presentClients.forEach(client => {
                console.log(client);
                clientCtlr.sendMailTo(client);
            });
        }
        res.send(advertisement);

    } catch (err) {
        console.log(err)
        res.sendStatus(500);
    }

};

exports.delete = function (req, res) {
    logger.log('DELETE ' + req.params.id);
    advertisementBuilder.delete(req.params.id).exec((err) => {
        if (err) return logger.log(err);
        res.send('Ok');
    });
    res.send('DELETE');
};
