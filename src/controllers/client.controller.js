
let clientBuilder = require('../builders/client.builder');
let adsBuilder = require('../builders/advertisement.builder');
let mailService = require('../services/mail.service');
let bluetoothModule = require('../modules/bluetooth.module');
let socketModule = require('../modules/socket.module');

const Logger = require('../tools/logger');
let logger = new Logger('CLIENT');

// The number of milliseconds in one day
var ONE_DAY = 1000 * 60 * 60 * 24;

exports.sendMailTo= function(client) {
   return advertise(client);
};

async function advertise(client){
    try {
        if (!client || !client.mail) return;
        let difference_ms = Math.abs(client.last_advertise - new Date());
        if (Math.round(difference_ms / ONE_DAY) > 1) {
            let advertisements = await adsBuilder.getActive();
            console.log('ads', advertisements);
            let text = '';

            text += 'Bonjour ' + client.prenom + ' ' + client.nom + '\r';
            text += 'Aujourd\'hui nous vous proposons comme promotions: \r';
            advertisements.forEach(ad => {
                console.log(ad);
                text += ad.message + '\r';
            });
            await mailService.send(client.mail, text);
            client.last_advertise = new Date();
            await clientBuilder.update(client);
            socketModule.send('advertised', {mac_address: client.mac_address, last_advertise: client.last_advertise})
        }
    } catch (err) {
        logger.logError('ADVERTISEMENT ' + err);
    }
}

exports.getByMacAddress = async function (mac_address) {
    try {
        await clientBuilder.getByMacAddress(mac_address)
    } catch (err) {
        console.log(err)
    }
};

exports.newTarget = async function(data){
    try{
        let client = await clientBuilder.getByMacAddress(data.mac_address);
        if (client) {
            data.name = client.name;
            data.mail = client.mail;
            data.last_advertise = client.last_advertise;
        }
        socketModule.send('newTarget', data);
        if(client) advertise(client);
    }catch (err) {
        logger.logError(err);
        throw err;
    }
};

exports.list = function (req, res) {
    logger.log('LIST');
    clientBuilder.listAll().exec((err, clients) => {
        if (err) return logger.logError(err);
        res.send(clients);
    });
};


exports.getListOfPresent = async function (req, res) {
    try {
        logger.log('LIST PRESENT');
        let targets = bluetoothModule.getDatas();
        let macAdresses = [];
        for (let macAddress in targets) {
            if (targets.hasOwnProperty(macAddress)) {
                macAdresses.push(macAddress);
            }
        }
        let clients = await clientBuilder.getByManyMacAddress(macAdresses);
        clients.forEach(client => {
            client.device_name = targets[client.mac_address].name;
        });

        res.send(clients);
    } catch (err) {
        logger.logError(err);
        res.sendStatus(500);
    }
};

exports.listPresent = async function () {
    try {
        logger.log('LIST PRESENT');
        let targets = bluetoothModule.getDatas();
        let macAdresses = [];
        for (let macAddress in targets) {
            if (targets.hasOwnProperty(macAddress)) {
                macAdresses.push(macAddress);
            }
        }
        let clients = await clientBuilder.getByManyMacAddress(macAdresses);
        clients.forEach(client => {
            if(targets[client.mac_address]){
                targets[client.mac_address].nom = client.nom;
                targets[client.mac_address].prenom = client.prenom;
                targets[client.mac_address].mail = client.mail;
                targets[client.mac_address].last_advertise = client.last_advertise;
            }
        });
        return targets;
    } catch (err) {
        throw err;
    }
};

exports.read = function (req, res) {
    logger.log('READ');
    clientBuilder.read(req.params.id).exec((err, client) => {
        if (err) return logger.logError(err);
        res.send(client);
    });
};

exports.create = function (req, res) {
    logger.log('CREATE');
    let newClient = req.body;
    clientBuilder.create(newClient, (err, client) => {
        if (err) return logger.logError(err);
        let text = "Bienvenue " + client.prenom;
        mailService.send(client.mail, text);
        res.send(client);
    });
};

exports.update = async function (req, res) {
    logger.log('UPDATE ' + req.params.id);
    let updatedClient = req.body;
    try{
      let client = await clientBuilder.update(updatedClient);
      res.send(client);
    }catch (err) {
      res.sendStatus(500);
    }

};

exports.delete = function (req, res) {
    logger.log('DELETE ' + req.params.id);
    clientBuilder.delete(req.params.id).exec((err) => {
        if (err) logger.logError(err);
        console.log('youhou');
        res.sendStatus(200);
    });
};


