let magasinBuilder = require('../builders/magasin.builder');

exports.list = function(req, res){
    log('LIST');
    magasinBuilder.listAll().exec((err, magasins)=>{
        if(err) return log(err);
        res.send(magasins);
    });
};

exports.read = function(req, res){
    log('READ');
    magasinBuilder.read(req.params.id).exec((err, magasin)=>{
        if(err) return log(err);
        res.send(magasin);
    });
};


exports.create = function(req, res){
    log('CREATE');
    let newMagasin = req.body;
    magasinBuilder.create(newMagasin, (err, magasin) => {
        if(err) return log(err);
        res.send(magasin);
    });
};

exports.update = function(req, res){
    log('UPDATE ' + req.params.id);
    let updatedMagasin = req.body;
    magasinBuilder.update(updatedMagasin).exec((err, magasin)=> {
        if(err) return log(err);
        res.send(magasin);
    });
};

exports.delete = function(req, res){
    log('DELETE ' + req.params.id);
    magasinBuilder.delete(req.params.id).exec((err)=> {
        if(err) return log(err);
        res.send('Ok');
    });
    res.send('DELETE');
};

log = function(msg){
    console.log('[Magasin] ' + msg);
};