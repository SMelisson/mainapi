let mongoose = require('mongoose');
let Magasin = mongoose.model('Magasin');

exports.listAll = function(){
    return Magasin.find({})
        .select('zones magasin')
        .populate('zones')
};

exports.create = function(newMagasin, callback){
    Magasin.create(newMagasin, (err, magasin)=>{
        callback(err,magasin);
    });
};

exports.read = function(id){
    return Magasin.findById(id)
};

exports.update = function(updatedMagasin){
    return Magasin.findByIdAndUpdate(updatedMagasin.id, updatedMagasin);
};

exports.delete = function(id){
    return Magasin.findByIdAndDelete(updatedMagasin.id);
};