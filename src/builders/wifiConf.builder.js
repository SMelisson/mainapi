/**
 * Created by Slvnm on 11/11/2018.
 */
let mongoose = require('mongoose');
let WifiConf = mongoose.model('WifiConf');

async function read(){
    return WifiConf.findOne().exec();
}

module.exports = {
    read
};