let mongoose = require('mongoose');
let Client = mongoose.model('Client');

exports.listAll = function () {
    return Client.find({})
};

exports.create = function (newClient, callback) {
    Client.create(newClient, (err, client) => {
        callback(err, client);
    });
};

exports.read = function (id) {
    return Client.findById(id)
};

exports.update = function (updatedClient) {
    return new Promise((resolve, reject) => {
        let id = updatedClient._id;
        delete updatedClient._id;
        console.log(updatedClient);
        Client.findOneAndUpdate(id, updatedClient,{new: true})
            .exec((err, client) => {
                if (err) reject(err);
                else {
                    resolve(client);
                }
            });
    });
};

exports.delete = function (id) {
    return Client.findByIdAndDelete(id);
};

exports.getByMacAddress = function (mac_address) {
    return new Promise((resolve, reject) => {
        Client.findOne({
            mac_address: mac_address
        }).exec((err, client) => {
            if (err) reject(err);
            else resolve(client);
        });
    })
};

exports.getByManyMacAddress = function (mac_adresses) {
    console.log(mac_adresses);
    return new Promise((resolve, reject) => {
        Client.find({
            mac_address: {
                $in: mac_adresses
            }
        }).exec((err, clients) => {
            if (err) reject(err);
            else resolve(clients);
        });
    })
};
