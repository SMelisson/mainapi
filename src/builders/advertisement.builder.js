let mongoose = require('mongoose');
let Advertisement = mongoose.model('Advertisement');

exports.listAll = function () {
    return Advertisement.find({})
};

exports.getActive = function () {
    return Advertisement.find({'isActive': true})
};

exports.create = function (newAdvertisement, callback) {
    Advertisement.create(newAdvertisement, (err, Advertisement) => {
        callback(err, Advertisement);
    });
};

exports.read = function (id) {
    return Advertisement.findById(id)
};

exports.update = function (updatedAdvertisement) {
    return new Promise((resolve, reject) => {
        let id = updatedAdvertisement._id;
        delete updatedAdvertisement._id;
        console.log(updatedAdvertisement);
        Advertisement.findByIdAndUpdate(id, updatedAdvertisement, {new: true})
            .exec((err, Advertisement) => {
                if (err) reject(err);
                else {
                    resolve(Advertisement);
                }
            });
    });
};

exports.delete = function (id) {
    return Advertisement.findByIdAndDelete(id);
};
