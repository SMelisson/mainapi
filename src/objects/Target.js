/**
 * Created by Slvnm on 25/11/2018.
 */
class Target{

    constructor(macAdress, rssi, name = 'N/C', txPowerLevel = -59){
        this.macAdress = macAdress;
        this.rssi = rssi;
        this.isPresent = true;
        this.txPowerLevel = txPowerLevel;
        this.name = name;
    }

    getName(){
        return this.name;
    }

    getMacAddress() {
        return this.macAdress;
    }

    getRssi() {
        return this.rssi;
    }

    setMacAddress(value) {
        this.macAdress = value;
    }

    setRssi(value) {
        this.rssi = value;
    }

    getIsPresent() {
       return this.isPresent;
    }

    setIsPresent(value) {
        this.isPresent = value;
    }

    setTxPowerLevel(value){
        this.txPowerLevel = value;
    }

    getDistance(){
        let txPower;
        if(this.txPowerLevel === (null || undefined)){
            txPower = -59;
        }else{
            txPower = this.txPowerLevel;
        }

        if (this.rssi === 0) {
            return -1.0;
        }

        let ratio = this.rssi * 1.0 / txPower;
        let distance;
        if (ratio < 1.0) {
            distance = Math.pow(ratio, 10);
        }
        else {
            distance = (0.89976) * Math.pow(ratio, 7.7095) + 0.111;
        }
        return Math.round(distance*10)/10;
    }

}

module.exports = Target;
