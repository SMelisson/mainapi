const axios = require('axios');
const {DATA_PATH} = require('../config/config');

class Beacon{

    constructor(nom, ip_address, mac_address, coordinates, connected){
        this.ip_address = ip_address;
        this.mac_address = mac_address;
        this.coordinates = coordinates;
        this.connected = connected;
        this.socket = null;
    }

    connect(socket){
        this.socket = socket;
        this.connected = true;
    }

    askDatas(path){
        return axios.get(this.ip_address + path);
    }
}

module.exports = Beacon;
