module.exports = function(app){

    let magasinCtrl = require('../controllers/magasin.controller');

    app.get('/magasins', magasinCtrl.list);
    app.get('/magasin/:id', magasinCtrl.read);
    app.post('/magasin', magasinCtrl.create);
    app.put('/magasin/:id', magasinCtrl.update);
    app.delete('/magasin/:id', magasinCtrl.delete);

};