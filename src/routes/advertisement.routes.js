module.exports = function(app){

    let advertisementCtrl = require('../controllers/advertisement.controller');

    app.get('/advertisements', advertisementCtrl.list);
    app.get('/advertisement/:id', advertisementCtrl.read);
    app.post('/advertisement', advertisementCtrl.create);
    app.put('/advertisement/:id', advertisementCtrl.update);
    app.delete('/advertisement/:id', advertisementCtrl.delete);

};
