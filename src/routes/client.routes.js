module.exports = function(app){

    let clientCtrl = require('../controllers/client.controller');

    app.get('/clients', clientCtrl.list);
    app.get('/clients/connected', clientCtrl.getListOfPresent);
    app.get('/client/:id', clientCtrl.read);
    app.post('/client', clientCtrl.create);
    app.put('/client/:id', clientCtrl.update);
    app.delete('/client/:id', clientCtrl.delete);

};
