var nodemailer = require('nodemailer');

exports.send = function(mail , message){
    console.log('Sending MAIL');
    return new Promise(async (resolve, reject) => {


        let transporter = nodemailer.createTransport({
            host: 'smtp.gmail.com',
            port: 465,
            secure: true,
            auth: {
                user: 'sm.projetbluetooth@gmail.com',
                pass: 'Projet59Bluetooth'
            }
        });

        var mailOptions = {
            from: 'sm.projetbluetooth@gmail.com',
            to: mail,
            subject: 'Nouvelles promotions',
            text: message
        };

        transporter.sendMail(mailOptions, function (err, info) {
            if (err) {
                reject(err);
            } else {
                resolve(console.log('Email sent: ' + info.response));
            }
        });
    });
};

