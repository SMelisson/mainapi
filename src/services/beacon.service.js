let axios = require('axios');
const Logger = require('../tools/logger');
let logger = new Logger('BEACON SERVICE');

exports.collectDatas = async function (beacons) {

        let promises = [];
        logger.log('Asking datas');
        let path = '/bluetoothDatas';

        let selfBeaconPromise = axios.get(process.env.BLUETOOTH_API + path);
        promises.push(selfBeaconPromise);

        for (let beacon of beacons) {
            if (beacon.connected) promises.push(beacon.askDatas(path));
        }

        return new Promise(async (resolve, reject) => {
            try {
                const res = await Promise.all(promises);
                logger.logDone('Datas received');
                resolve(res);
            } catch (err) {
                logger.logError(err);
                reject(err);
            }
        });
};
