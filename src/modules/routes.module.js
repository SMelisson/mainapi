const Logger = require('../tools/logger');
let logger = new Logger('ROUTES MODULE');

module.exports = function (app) {

    logger.logDone('INITIALISED');

    let magasinRoute = require('../routes/magasin.routes');
    magasinRoute(app);

    let clientRoute = require('../routes/client.routes');
    clientRoute(app);

    let advertisementRoute = require('../routes/advertisement.routes');
    advertisementRoute(app);
};
