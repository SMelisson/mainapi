let mongoose = require('mongoose');
const Logger = require('../tools/logger');
let logger = new Logger('DB MODULE');

exports.init = async function(){
    logger.log('DB MODULE CONNECTING TO ' + "mongodb://mongo:27017/app_db");
    return reconnect();
};

async function reconnect(){
    return new Promise((resolve, reject) => {
        mongoose.connect('mongodb://mongo:27017/app_db',  { useNewUrlParser: true } , (err,  db) => {
            if(err){
                logger.logError('CONNECTION FAILED, ATTEMPTING IN 5s');
                reject(err.message);
            } else if(db){
                logger.logDone('DB CONNECTION TO ' + db.name + ' ESTABLISHED');
                resolve();
            }
        });
    });

}
