const Logger = require('../tools/logger');
const logger = new Logger('BLUETOOTH MODULE');
const Target = require('../objects/Target');
let targets = {};

const clientCtrl = require('../controllers/client.controller');
const socketModule = require('./socket.module');

const blue = require("bluetoothctl");
blue.Bluetooth();

exports.init = async function () {
    logger.log('init');
    return new Promise((resolve, reject) => {
        blue.on(blue.bluetoothEvents.Controller, function (controllers) {
            if (hasBluetooth) {
                resolve(logger.logDone('SCAN SET TO ON'));
                blue.scan(true);
                follow(30000);
            }
        });

        blue.on(blue.bluetoothEvents.DeviceSignalLevel, function (devices, mac, signal) {
            targets[mac].setRssi(signal);
            socketModule.send('rssiRefresh', {mac_address: mac, rssi: signal});
        });

        blue.on(blue.bluetoothEvents.Device, function (devices) {
            devices.forEach(device => {
                if (targets[device.mac] === undefined) {
                    let name = device.name.includes('ManufacturerData') ? 'N/C' : device.name;
                    logger.log("new Target " + device.mac + " " + name);
                    targets[device.mac] = new Target(device.mac, device.signal, name);

                    let data = {
                        mac_address: targets[device.mac].getMacAddress(),
                        device_name: targets[device.mac].getName()
                    };

                    clientCtrl.newTarget(data);

                } else {
                    targets[device.mac].setIsPresent(true);
                }
            });
        });

        var hasBluetooth = blue.checkBluetoothController();

        process.on('SIGINT', function () {
            logger.logDone('SCAN SET TO OFF');
            blue.scan(false);
            blue.exit();
            process.exit();
        });

        resolve();
    });
};

function follow(int) {
    setInterval(() => {
        for (let macAddress in targets) {
            if (targets.hasOwnProperty(macAddress)) {
                blue.remove(macAddress);
                if (!targets[macAddress].getIsPresent()) {
                    logger.log('Unfollow : ' + macAddress);
                    socketModule.send('targetUnFollowed', macAddress);
                    delete targets[macAddress];
                }
                if (targets[macAddress] !== undefined) {
                    targets[macAddress].setIsPresent(false);
                }
            }
        }
    }, int);
}

exports.getDatas = function () {
    let clients = [];
    for(let target in targets){
        if(targets.hasOwnProperty(target))
        clients.push({device_name: targets[target].getName(), mac_address: targets[target].getMacAddress()});
    }
    return clients;
};

