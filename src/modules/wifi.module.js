**
 * Created by Slvnm on 11/11/2018.
 */
let isDeployed;
let Logger = require('../tools/logger');
let logger = new Logger('WIFI MODULE');
let wifi = require('node-wifi');
const {SSID, HOTSPOT_PASSWORD} = require('../config/config');

const wifiConfBldr = require('../builders/wifiConf.builder');

async function init() {
    isDeployed = false;
    logger.log('INIT');
    try {
        wifi.init();

        let wifiConf = await wifiConfBldr.read();
        if (wifiConf === null) {
            logger.log('NO CONFIG FOUND');
        }
        let connected = await connectTo(SSID, HOTSPOT_PASSWORD);
        if (!connected) await deployHotSpot();
    } catch (err) {
        return ('BLUETOOTH MODULE ' + err);
    }

}

async function connectTo(ssid, password) {
    wifi.init();
    return new Promise((resolve, reject) => {
        wifi.scan().then(function (networks) {
            // networks
            let network = networks.find(net => net.ssid === ssid);
            if (network !== null && network !== undefined) {
                logger.log('CONNECTING TO ' + ssid);
                wifi.connect({ssid: ssid, password: password}, function (err) {
                    if (err)
                        reject(err);
                    logger.logDone('CONNECTED');
                    resolve(true);
                });
            } else {
                logger.log(ssid + ' NOT REACHABLE');
                resolve(false);
            }
        }).catch(function (error) {
            // error
            logger.logError('CONNECTION FAILURE :' + error);
            reject(error);
        })
    });
}

async function deployHotSpot() {
    try {
        logger.log('DEPLOYING HOTSPOT');
    } catch (err) {
        logger.logError('DEPLOYING HOTSPOT' + err);
    }
}

module.exports = {
    init,
    deployHotSpot
};
