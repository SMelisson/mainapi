const Logger = require('../tools/logger');
let logger = new Logger('SOCKET MODULE');

const clientCtrl = require('../controllers/client.controller');
const io = require('socket.io');
let ioServer = io.listen(8900);

let userSpace = ioServer.of('/user');
let userCount = 0;

exports.init = async function () {

    ioServer.on('connection', () => {
        logger.log('CONNECTION');
    });

    return new Promise(async (resolve, reject) => {

        userSpace.on('connection', async (socket) => {
            logger.log('User ' + socket.request.connection.remoteAddress + ' connected');
            let clients = await clientCtrl.listPresent();
            socket.emit('targets', clients);
            userCount++;

            socket.on('disconnect', () => {
                logger.log('User ' + socket.handshake.mac_address + ' disconnected');
                userCount--;
            });
        });

        resolve(logger.logDone('INITIALISED ON PORT ' + 8900));
    });
};

exports.send = function (event, data) {
    userSpace.emit(event, data);
};
