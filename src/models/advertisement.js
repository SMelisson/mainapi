let mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Advertisement = new Schema({
    name: String,
    message: String,
    isActive: {type: Boolean, default: false}
});

module.exports = mongoose.model('Advertisement', Advertisement);
