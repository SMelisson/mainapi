let mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Client = new Schema({
    nom:  String,
    prenom:  String,
    adhesion: {type: Date, default : Date.now},
    birthday: {type: Date},
    mail: String,
    phone: String,
    mac_address: String,
    preferences: String,
    last_advertise: {type: Date, default : Date.now},
    gen_freq: Number,
    freq: Number
});

module.exports = mongoose.model('Client', Client );
