let mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Magasin = new Schema({
    title: String,
    zones: [{type: mongoose.Schema.Types.ObjectId, ref: 'Zone'}]
});

module.exports = mongoose.model('Magasin', Magasin );