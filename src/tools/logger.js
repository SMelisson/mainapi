module.exports = class Logger{

    constructor(serviceName){
       this.serviceName = serviceName;
    }

    log(msg){
        return console.log("\x1b[0m", ' ['+ this.serviceName.toUpperCase() +`] ${msg}`, '\x1b[0m')
    }

    logError(msg){
        process.stdout.write('  ['+ this.serviceName.toUpperCase() +']');
        return console.log('\x1b[31m', '\u2613 ERROR ' + msg, '\x1b[0m');
    }

    logDone(msg){
        process.stdout.write('  ['+ this.serviceName.toUpperCase() +']');
        return console.log('\x1b[32m', '\u2714 ' + msg, '\x1b[0m');
    }

};
