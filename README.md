version: "2"
services:
  main-api:
    container_name: main-api
    restart: always
    build: mainapi/.
    ports:
      - "3000:3000"
      - "8900:8900"
    environment:
      - MONGO_URI=mongodb://mongo:27017/app_db
      - RHOST=redis
      - BLUETOOTH_API=bluetooth-api
    links:
      - mongo
      - redis:redis
    depends_on:
      - mongo
      - redis
  bluetooth-api:
    container_name: bluetooth-api
    build: bluetoothapi/.
    environment:
      - MAIN_API=http://localhost:3040
    privileged: true
    network_mode: host
    restart: always
    depends_on:
          - main-api
  redis:
    container_name: redis
    image: redis:alpine
    command: ["redis-server", "--appendonly", "yes"]
    hostname: redis
    volumes:
        - ./data:/data/redis
    logging:
          driver: "none"
  mongo:
    container_name: mongo
    image: mongo
    volumes:
      - ./data:/data/db
    ports:
      - "27017:27017"
    logging:
          driver: "none"